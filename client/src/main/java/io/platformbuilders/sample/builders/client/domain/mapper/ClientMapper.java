package io.platformbuilders.sample.builders.client.domain.mapper;

import io.platformbuilders.sample.builders.client.domain.dto.ClientInsertRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientResponse;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSavePartiallyRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSaveRequest;
import io.platformbuilders.sample.builders.client.domain.entity.ClientEntity;
import io.platformbuilders.sample.builders.commons.parser.AutoMapper;
import io.platformbuilders.sample.builders.commons.utils.DateUtils;
import io.platformbuilders.sample.builders.commons.utils.PagedResult;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.Optional;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ClientMapper {

    public static ClientResponseMapper CLIENT_RESPONSE_MAPPER = new ClientResponseMapper();
    public static ClientInsertMapper CLIENT_INSERT_MAPPER = new ClientInsertMapper();
    public static ClientSaveRequestMapper CLIENT_SAVE_MAPPER = new ClientSaveRequestMapper();
    public static ClientSavePartiallyRequestMapper CLIENT_SAVE_PARTIALLY_MAPPER = new ClientSavePartiallyRequestMapper();

    public static class ClientResponseMapper extends AutoMapper<ClientResponse, ClientEntity> {

        @Override
        public ClientResponse fromEntity(ClientEntity entity) {
            ClientResponse clientResponse = super.fromEntity(entity);
            clientResponse.setAge(DateUtils.calculateAgeByDate(entity.getBirthDate()));
            return clientResponse;
        }

        public PagedResult<ClientResponse> fromPageEntities(Page<ClientEntity> clients) {
            PagedResult<ClientResponse> pagedResult = new PagedResult<>();
            pagedResult.setTotalPages(clients.getTotalPages());
            pagedResult.setTotalElements(clients.getTotalElements());
            pagedResult.setCurrentPage(clients.getNumber());
            pagedResult.setResults(clients.getContent().stream().map(c -> fromEntity(c)).collect(Collectors.toList()));
            return pagedResult;
        }
    }

    public static class ClientSavePartiallyRequestMapper extends AutoMapper<ClientSavePartiallyRequest, ClientEntity> {

        public ClientEntity toEntity(ClientSavePartiallyRequest saveRequest, ClientEntity clientEntity) {
            Optional.ofNullable(saveRequest.getName()).ifPresent(c -> clientEntity.setName(c));
            Optional.ofNullable(saveRequest.getFederalId()).ifPresent(c -> clientEntity.setFederalId(c));
            Optional.ofNullable(saveRequest.getBirthDate()).ifPresent(c -> clientEntity.setBirthDate(c));
            return clientEntity;
        }
    }

    public static class ClientSaveRequestMapper extends AutoMapper<ClientSaveRequest, ClientEntity> {

    }

    public static class ClientInsertMapper extends AutoMapper<ClientInsertRequest, ClientEntity> {

    }
}
