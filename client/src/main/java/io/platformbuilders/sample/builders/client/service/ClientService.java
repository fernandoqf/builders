package io.platformbuilders.sample.builders.client.service;

import io.platformbuilders.sample.builders.client.domain.dto.ClientInsertRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientResponse;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSavePartiallyRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSaveRequest;
import io.platformbuilders.sample.builders.commons.utils.PagedResult;
import org.springframework.data.domain.Pageable;

public interface ClientService {

    ClientResponse findById(Long id);
    PagedResult<ClientResponse> findAllBy(String federalId, String name, Integer page, Integer pageSize, String sortBy);
    ClientResponse insert(ClientInsertRequest insertRequest);
    ClientResponse save(Long id, ClientSaveRequest saveRequest);
    ClientResponse savePartially(Long id, ClientSavePartiallyRequest saveRequest);
    void delete(Long id);
}
