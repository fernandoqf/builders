package io.platformbuilders.sample.builders.client.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientInsertRequest {

    @NotEmpty(message = "{client.name.required}")
    @JsonProperty("nome")
    private String name;

    @CPF(message = "{client.cpf.invalid}")
    @NotEmpty(message = "{client.cpf.required}")
    @JsonProperty("cpf")
    private String federalId;

    @NotNull
    @JsonProperty("dataNascimento")
    private LocalDate birthDate;
}
