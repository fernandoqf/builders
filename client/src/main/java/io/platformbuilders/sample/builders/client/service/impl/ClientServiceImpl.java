package io.platformbuilders.sample.builders.client.service.impl;

import io.platformbuilders.sample.builders.client.domain.dto.ClientInsertRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientResponse;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSavePartiallyRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSaveRequest;
import io.platformbuilders.sample.builders.client.domain.entity.ClientEntity;
import io.platformbuilders.sample.builders.client.domain.enums.ClientError;
import io.platformbuilders.sample.builders.client.domain.mapper.ClientMapper;
import io.platformbuilders.sample.builders.client.repository.ClientRepository;
import io.platformbuilders.sample.builders.client.service.ClientService;
import io.platformbuilders.sample.builders.commons.exception.NotFoundException;
import io.platformbuilders.sample.builders.commons.utils.PagedResult;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;

    @Override
    public ClientResponse findById(Long id) {

        Optional<ClientEntity> clientEntity = clientRepository.findById(id);
        if (!clientEntity.isPresent()) {
            throw new NotFoundException(ClientError.CLIENT_NOT_FOUND, id);
        }
        return ClientMapper.CLIENT_RESPONSE_MAPPER.fromEntity(clientEntity.get());
    }

    @Override
    public PagedResult<ClientResponse> findAllBy(String federalId, String name, Integer page, Integer pageSize, String sortBy) {

        name = name != null ? String.format("%%%s%%", name) : null;
        federalId = federalId != null ? String.format("%%%s%%", federalId) : null;

        Pageable pageable = PageRequest.of(page, pageSize, Sort.by(sortBy));
        Page<ClientEntity> clients = clientRepository.findAllBy(federalId, name, pageable);
        return ClientMapper.CLIENT_RESPONSE_MAPPER.fromPageEntities(clients);
    }

    @Override
    @Transactional
    public ClientResponse insert(ClientInsertRequest insertRequest) {

        ClientEntity clientEntity = ClientMapper.CLIENT_INSERT_MAPPER.toEntity(insertRequest);
        clientEntity = clientRepository.save(clientEntity);

        return ClientMapper.CLIENT_RESPONSE_MAPPER.fromEntity(clientEntity);
    }

    @Override
    @Transactional
    public ClientResponse save(Long id, ClientSaveRequest saveRequest) {

        Optional<ClientEntity> clientEntity = clientRepository.findById(id);
        if (!clientEntity.isPresent()) {
            throw new NotFoundException(ClientError.CLIENT_NOT_FOUND, id);
        }
        ClientEntity clientToSave = ClientMapper.CLIENT_SAVE_MAPPER.toEntity(saveRequest);
        clientToSave.setId(clientEntity.get().getId());
        clientToSave = clientRepository.save(clientToSave);

        return ClientMapper.CLIENT_RESPONSE_MAPPER.fromEntity(clientToSave);
    }

    @Override
    @Transactional
    public ClientResponse savePartially(Long id, ClientSavePartiallyRequest saveRequest) {

        Optional<ClientEntity> clientEntity = clientRepository.findById(id);
        if (!clientEntity.isPresent()) {
            throw new NotFoundException(ClientError.CLIENT_NOT_FOUND, id);
        }
        ClientEntity clientToSave = ClientMapper.CLIENT_SAVE_PARTIALLY_MAPPER.toEntity(saveRequest, clientEntity.get());
        clientToSave = clientRepository.save(clientToSave);

        return ClientMapper.CLIENT_RESPONSE_MAPPER.fromEntity(clientToSave);
    }

    @Override
    @Transactional
    public void delete(Long id) {

        Optional<ClientEntity> clientEntity = clientRepository.findById(id);
        if (!clientEntity.isPresent()) {
            throw new NotFoundException(ClientError.CLIENT_NOT_FOUND, id);
        }
        clientRepository.delete(clientEntity.get());
    }
}
