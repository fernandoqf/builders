package io.platformbuilders.sample.builders.client.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientSavePartiallyRequest {

    @JsonProperty("nome")
    private String name;

    @JsonProperty("cpf")
    private String federalId;

    @JsonProperty("dataNascimento")
    private LocalDate birthDate;
}
