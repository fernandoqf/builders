package io.platformbuilders.sample.builders.client.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientSaveRequest {

    @NotNull
    @NotEmpty
    @JsonProperty("nome")
    private String name;

    @CPF
    @NotNull
    @NotEmpty
    @JsonProperty("cpf")
    private String federalId;

    @NotNull
    @JsonProperty("dataNascimento")
    private LocalDate birthDate;
}
