package io.platformbuilders.sample.builders.client.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientResponse {

    private Long id;

    @JsonProperty("nome")
    private String name;

    @JsonProperty("cpf")
    private String federalId;

    @JsonProperty("dataNascimento")
    private LocalDate birthDate;

    @JsonProperty("idade")
    private Long age;
}
