package io.platformbuilders.sample.builders.client.repository;

import io.platformbuilders.sample.builders.client.domain.entity.ClientEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {

    @Query("select c from ClientEntity c where (:federalId is null or c.federalId like :federalId) "+
        " and (:name is null or c.name like :name) ")
    Page<ClientEntity> findAllBy(@Param("federalId") String federalId, @Param("name") String name, Pageable pageable);

}
