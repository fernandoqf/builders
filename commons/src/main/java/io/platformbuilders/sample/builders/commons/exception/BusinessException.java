package io.platformbuilders.sample.builders.commons.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Business exception @ Spring Core classes for Spring boot based project
 * @author Fernando Queiroz Fonseca
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    @Getter
    private Enum error;

    @Getter
    private String[] parameters;

    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(Enum error) {
        super(error.name());
        this.error = error;
    }

    public BusinessException(Enum error, String... parameters) {
        super(error.name());
        this.error = error;
        this.parameters = parameters;
    }

    public BusinessException(Enum error, Throwable cause) {
        super(cause);
        this.error = error;
    }

    public BusinessException(String message, Throwable cause) {
        super(message, cause);
    }

    public enum BusinessExceptionError {
        INTERNAL_SERVER_ERROR,
        BAD_REQUEST
    }
}