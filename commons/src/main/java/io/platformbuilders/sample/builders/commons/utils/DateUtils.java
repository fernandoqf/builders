package io.platformbuilders.sample.builders.commons.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtils {

    public static Long calculateAgeByDate(LocalDate date) {
        return date == null ? 0 : ChronoUnit.YEARS.between(date, LocalDate.now());
    }
}
