package io.platformbuilders.sample.builders.commons.exception;


import io.platformbuilders.sample.builders.commons.parser.MessageUtils;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Controller for handle with exceptions @ Spring Core classes for Spring boot based project
 * @author Fernando Queiroz Fonseca
 */
@Log4j2
@ControllerAdvice
@AllArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class GlobalExceptionHandler extends ValidationExceptionHandler {

    MessageUtils messageUtils;

    @Override
    public ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(body, ex);
        return buildResponseMessage(status.name(), ex.getLocalizedMessage(), status, request);
    }

    @Override
    public ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        log.error(request, ex);
        return buildResponseMessage(status.name(), ex.getLocalizedMessage(), status, request);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {

        log.error(request, ex);
        return buildResponseMessage("INTERNAL_SERVER_ERROR",
            getMessage(BusinessException.BusinessExceptionError.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(BusinessException.class)
    public final ResponseEntity<Object> handleBadRequestException(BusinessException ex, WebRequest request) {

        log.error(request, ex);
        String key = ex.getError() != null ? ex.getError().name() : "BUSINESS_ERROR";
        String message = ex.getError() != null ? getMessage(ex.getError(), ex.getParameters()) : ex.getLocalizedMessage();
        return buildResponseMessage(key, message, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<Object> handleBadRequestException(NotFoundException ex, WebRequest request) {

        log.error(request, ex);
        String key = ex.getError() != null ? ex.getError().name() : "NOT_FOUND";
        String message = ex.getError() != null ? getMessage(ex.getError(), ex.getParameters()) : ex.getLocalizedMessage();
        return buildResponseMessage(key, message, HttpStatus.NOT_FOUND, request);
    }


    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {

        log.error(request, ex);
        if (ex.getConstraintViolations() != null && !ex.getConstraintViolations().isEmpty()) {
            return ResponseEntity.ok(buildEntity(ex.getConstraintViolations(), request));
        } else {
            return buildResponseMessage("BAD_REQUEST",
                getMessage(BusinessException.BusinessExceptionError.BAD_REQUEST), HttpStatus.BAD_REQUEST, request);
        }
    }

    private String getMessage(Enum error, Object... parameters) {
        String message = messageUtils.getMessage(error.getDeclaringClass().getName().replace("$", ".") + "." + error.name(), parameters);
        return message == null ? "ERROR" : message;
    }

    private ResponseEntity<Object> buildResponseMessage(String errorKey, String localizedMessage, HttpStatus status, WebRequest request) {

        List<ResponseMessage> details = Arrays.asList(ResponseMessage.builder()
            .status(status.value())
            .error(errorKey)
            .timestamp(LocalDateTime.now())
            .path(((ServletWebRequest) request).getRequest().getRequestURI())
            .message(localizedMessage)
            .build());

        return new ResponseEntity(details, status);
    }

    /**
     * Build a list of response object for constraint validation {@link ResponseMessage}
     * @param violations {@link ValidationException}
     * @param request {@link WebRequest}
     * @return List of {@link ResponseMessage}
     */
    public List buildEntity(Set<ConstraintViolation<?>> violations, WebRequest request) {

        return violations
            .stream().filter(Objects::nonNull).map(cv -> {
                PathImpl path = (PathImpl) cv.getPropertyPath();
                String annotation = cv.getConstraintDescriptor().getAnnotation().annotationType().getName();
                String error = buildError(cv, annotation, path);
                return (ResponseMessage.builder()
                    .error(error)
                    .status(HttpStatus.BAD_REQUEST.value())
                    .message(buildMessage(cv))
                    .path(((ServletWebRequest) request).getRequest().getRequestURI())
                    .timestamp(LocalDateTime.now())
                    .build());
            }).collect(Collectors.toList());
    }

    /**
     * Construct error field based on annotation or enum
     * @param cv {@link ConstraintViolation}
     * @param annotation {@link String}
     * @param path {@link PathImpl}
     * @return error field {@link String}
     */
    private static String buildError(ConstraintViolation<?> cv, String annotation, PathImpl path) {

        try {
            if (cv.getMessageTemplate().startsWith("{")) {
                if (path.getLeafNode() != null
                    && path.getLeafNode().getName() != null) {
                    return path.getLeafNode().getName().toUpperCase() + "_INVALID";
                } else {
                    return "ERROR_" + annotation.substring(annotation.lastIndexOf(".") + 1).toUpperCase();
                }
            } else {
                return cv.getMessageTemplate().substring(cv.getMessageTemplate().lastIndexOf(".") + 1).toUpperCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return annotation;
        }
    }

    /**
     * Construct message field based message key
     * @param cv {@link ConstraintViolation}
     * @return message field {@link String}
     */
    private String buildMessage(ConstraintViolation<?> cv) {

        if (cv == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase();
        }

        try {
            String keyMessage = buildKeyMessage(cv.getMessageTemplate());
            String msg = messageUtils.getMessage(keyMessage);
            return StringUtils.isBlank(msg) ? cv.getMessage() : msg;
        } catch (Exception e) {
            e.printStackTrace();
            return cv.getMessage();
        }
    }

    /**
     * Check if is Validator or custom message (Validator is between {javax.any})
     * @param keyMessage Key identifier for message
     * @return if custom the key or else the value between {value}
     */
    private static String buildKeyMessage(String keyMessage) {
        return keyMessage.startsWith("{") ? keyMessage.substring(1, keyMessage.length()-1) : keyMessage;
    }

}