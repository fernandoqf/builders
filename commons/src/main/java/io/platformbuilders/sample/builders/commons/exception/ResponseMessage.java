package io.platformbuilders.sample.builders.commons.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Default Message response for generate Json Payload in validation and Errors
 * @author Fernando Queiroz Fonseca
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseMessage {

    private Integer status;
    private String error;
    private String message;
    private LocalDateTime timestamp;
    private String path;
}
