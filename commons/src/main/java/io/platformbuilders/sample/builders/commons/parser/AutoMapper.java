package io.platformbuilders.sample.builders.commons.parser;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import java.lang.reflect.ParameterizedType;

/**
 * Generic Auto-Mapper class for convert entity to DTO and vice-versa
 * @author Fernando Queiroz Fonseca
 */
@Component
@ApplicationScope
public class AutoMapper<DTO, E> extends ModelMapper {

    /**
     * Convert a DTO to Entity object
     * @param dto DTO object
     * @return converted Entity
     */
    public E toEntity(DTO dto) {
        return map(dto, getEntityTypeClass());
    }

    /**
     * Convert a entity object to DTO
     * @param entity Entity E
     * @return object DTO
     */
    public DTO fromEntity(E entity) {
        return map(entity, getDTOTypeClass());
    }

    /**
     * Get type class for instance DTO
     * @return Class<DTO>
     */
    @SuppressWarnings("unchecked")
    private Class<E> getEntityTypeClass() {
        return (Class<E>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[1];
    }

    /**
     * Get type class for instance DTO
     * @return Class<DTO>
     */
    @SuppressWarnings("unchecked")
    private Class<DTO> getDTOTypeClass() {
        return (Class<DTO>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

}
