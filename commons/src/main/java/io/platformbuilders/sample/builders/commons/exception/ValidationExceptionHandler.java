package io.platformbuilders.sample.builders.commons.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Handle with validation errors @ Spring Core classes for Spring boot based project
 * @author Fernando Queiroz Fonseca
 * @since 26/08/2020
 */
@Log4j2
public class ValidationExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    public ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        log.error(request, ex);
        return new ResponseEntity(buildErrorMessage(ex.getBindingResult(), request), headers, status);
    }

    @Override
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        log.error(request, ex);
        return new ResponseEntity<>(buildErrorMessage(ex.getBindingResult(), request), headers, status);
    }

    private List<ResponseMessage> buildErrorMessage(BindingResult bindingResult, WebRequest request) {
        List<ResponseMessage> validationErrors = new ArrayList<>();
        if (bindingResult.hasErrors()) {
            List<FieldError> fieldErrors = bindingResult.getFieldErrors();
            fieldErrors.stream().forEach(fieldError -> {
                validationErrors.add(ResponseMessage.builder()
                        .status(HttpStatus.BAD_REQUEST.value())
                        .error(fieldError.getField().toUpperCase() + "_INVALID")
                        .timestamp(LocalDateTime.now())
                        .path(((ServletWebRequest)request).getRequest().getRequestURI())
                        .message(fieldError.getDefaultMessage())
                        .build());
            });
        }
        return validationErrors;
    }

}
