package io.platformbuilders.sample.builders.commons.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Business exception @ Spring Core classes for Spring boot based project
 * @author Fernando Queiroz Fonseca
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    @Getter
    private Enum error;

    @Getter
    private Object[] parameters;

    public NotFoundException(Enum error, Object... parameters) {
        super(error.name());
        this.error = error;
        this.parameters = parameters;
    }

}
