package io.platformbuilders.sample.builders.commons.parser;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class MessageUtils {

    private MessageSource messageSource;

    public String getMessage(String key, Object... parameters) {
        return messageSource.getMessage(key, parameters, LocaleContextHolder.getLocale());
    }
}
