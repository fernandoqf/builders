package io.platformbuilders.sample.builders.commons.utils;

import lombok.Data;

import java.util.List;

@Data
public class PagedResult<T> {

    private List<T> results;
    private int currentPage;
    private int totalPages;
    private long totalElements;
}
