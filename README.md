# README #

Projeto de API em Spring Boot com Maven (multi-modulo), com CRUD para operações de Cliente.

### Passos para execução do Projeto ###

* Clonando repositório
> git clone https://bitbucket.org/fernandoqf/builders.git

* Instalando Docker:
É necessário instalar o docker-ce e docker-compose para executar o banco de dados e a aplicação a partir do Docker

Linux Ubuntu/Debian:
> sudo apt-get install docker-ce docker-compose

Outros sistemas download em:
> https://www.docker.com/products/docker-desktop

* Executando MySQL Database a partir do Docker:
> cd docker/mysql  
> sudo docker-compose up

* Compilando projeto utilizando Maven
> ./mvnw clean install

* Gerando imagem Docker da Aplicação:
> cd api  
> sudo docker build -t builders . 

* Executando com Docker
> cd docker/builders  
> sudo docker-compose up


### Executando testes com Postman ###

* Importar coleção do diretório:
> config/Builders.postman_collection.json
* Importando Environment (Profile):
> config/Fernando (Builders).postman_environment.json

### Executando testes com CURL ###

* Consultar clientes com paginação e filtros (cpf/nome):
> curl --location --request GET 'http://localhost:8085/cliente?cpf=0&nome=Fernando&pageSize=1&page=0'

* Consultar cliente pelo identificador:
> curl --location --request GET 'http://localhost:8085/cliente/1'

* Inserir um novo cliente:
> curl --location --request POST 'http://localhost:8085/cliente' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nome": "Fernando Queiroz Fonseca",
    "cpf": "01234567890",
    "dataNascimento": "1982-12-30"
}'

* Salvar um cliente:
> curl --location --request POST 'http://localhost:8085/cliente/1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nome": "Fernando Fonseca",
    "cpf": "01234567890",
    "dataNascimento": "1982-12-30"
}'

* Salvar cliente parcialmente:
> curl --location --request PATCH 'http://localhost:8085/cliente/1' \
--header 'Content-Type: application/json' \
--data-raw '{
    "nome": "Fernando Queiroz Fonseca"
}'

* Deletar um cliente:
> curl --location --request DELETE 'http://localhost:8085/cliente/1'

### Padrões ###
* Formato padrão de retorno de erro
```json
[
    {
        "status": 404,
        "error": "CLIENT_NOT_FOUND",
        "message": "Cliente com o identificador 1 não foi encontrado!",
        "timestamp": "2020-12-29T19:08:06.948368",
        "path": "/cliente/1"
    }
]
```

### Documentação Swagger

* Após executar o projeto acesse a documentação no formato Swagger (OpenAPI) no endereço: 
> http://localhost:8085/swagger-ui.html


## Informações
> Fernando Queiroz Fonseca - fernandoqf@msn.com
