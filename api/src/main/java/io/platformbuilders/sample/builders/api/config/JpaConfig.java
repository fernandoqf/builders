package io.platformbuilders.sample.builders.api.config;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories("io.platformbuilders.sample.builders")
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
public class JpaConfig {

}
