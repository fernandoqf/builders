package io.platformbuilders.sample.builders.api.client;

import io.platformbuilders.sample.builders.client.domain.dto.ClientInsertRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientResponse;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSavePartiallyRequest;
import io.platformbuilders.sample.builders.client.domain.dto.ClientSaveRequest;
import io.platformbuilders.sample.builders.client.service.ClientService;
import io.platformbuilders.sample.builders.commons.exception.ResponseMessage;
import io.platformbuilders.sample.builders.commons.utils.PagedResult;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@AllArgsConstructor
@RequestMapping("/cliente")
public class ClientAPI {

    private ClientService clientService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Consulta um cliente")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
        @ApiResponse(code = 400, message = "Erro de validação ou recuperação de informações", response = ResponseMessage[].class),
        @ApiResponse(code = 404, message = "Não encontrado", response = ResponseMessage[].class),
        @ApiResponse(code = 500, message = "Ocorreu um erro interno", response = ResponseMessage[].class)
    })
    public @ResponseBody ClientResponse findById(@NotNull @PathVariable("id") Long id) {
        return clientService.findById(id);
    }

    @GetMapping
    @ApiOperation(value = "Consulta todos cliente com filtro e paginação" )
    @ApiResponses({
        @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
        @ApiResponse(code = 400, message = "Erro de validação ou recuperação de informações", response = ResponseMessage[].class),
        @ApiResponse(code = 500, message = "Ocorreu um erro interno", response = ResponseMessage[].class)
    })
    public @ResponseBody PagedResult<ClientResponse> findAllBy(@RequestParam(name = "cpf", required = false) String federalId,
                                          @RequestParam(name = "nome", required = false) String name,
                                          @RequestParam(name = "page", defaultValue = "0") Integer page,
                                          @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                          @RequestParam(name = "sortBy", defaultValue = "id") String sortBy) {

        return clientService.findAllBy(federalId, name, page, pageSize, sortBy);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deleta um cliente")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
        @ApiResponse(code = 400, message = "Erro de validação ou recuperação de informações", response = ResponseMessage[].class),
        @ApiResponse(code = 404, message = "Não encontrado", response = ResponseMessage[].class),
        @ApiResponse(code = 500, message = "Ocorreu um erro interno", response = ResponseMessage[].class)
    })
    public @ResponseBody void delete(@NotNull @PathVariable("id") Long id) {

        clientService.delete(id);
    }

    @PostMapping
    @ApiOperation(value = "Insere um cliente")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
        @ApiResponse(code = 400, message = "Erro de validação ou recuperação de informações", response = ResponseMessage[].class),
        @ApiResponse(code = 500, message = "Ocorreu um erro interno", response = ResponseMessage[].class)
    })
    public @ResponseBody ClientResponse insert(@RequestBody @Valid ClientInsertRequest insertRequest) {

        return clientService.insert(insertRequest);
    }

    @PostMapping("/{id}")
    @ApiOperation(value = "Salva um cliente")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
        @ApiResponse(code = 400, message = "Erro de validação ou recuperação de informações", response = ResponseMessage[].class),
        @ApiResponse(code = 404, message = "Não encontrado", response = ResponseMessage[].class),
        @ApiResponse(code = 500, message = "Ocorreu um erro interno", response = ResponseMessage[].class)
    })
    public @ResponseBody ClientResponse save(@NotNull @PathVariable("id") Long id,
                                             @Valid @RequestBody ClientSaveRequest saveRequest) {

        return clientService.save(id, saveRequest);
    }

    @PatchMapping("/{id}")
    @ApiOperation(value = "Salva um cliente parcialmente")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Operação realizada com sucesso"),
        @ApiResponse(code = 400, message = "Erro de validação ou recuperação de informações", response = ResponseMessage[].class),
        @ApiResponse(code = 404, message = "Não encontrado", response = ResponseMessage[].class),
        @ApiResponse(code = 500, message = "Ocorreu um erro interno", response = ResponseMessage[].class)
    })
    public @ResponseBody ClientResponse savePartially(@NotNull @PathVariable("id") Long id,
                                                      @NotNull @RequestBody ClientSavePartiallyRequest saveRequest) {

        return clientService.savePartially(id, saveRequest);
    }
}
